<!DOCTYPE html>
<html>
    <head>
        <meta chaset="UTF-8">
        <title>Initializing CreateLeaner</title>
		<link rel="stylesheet" type="text/css" href="style.css" />
    </head>
    <body>
	<?php
	$footer = "Version B-2.0";//version for current php app
	?>
	<div id="wrapper">
		<div id="header">
			<h2><pre>Building <font color ="blue">CreateLearner</font> API-SOAP Request:</pre></h2>
		</div><!-- #header -->
		<hr>
		<div id="content"></br>
			<form id="post-form" method="POST">
				<div>
					<p id="url">Parameters are case sensitive</p>
					<label id ="url" for="url">Enter<strong> WSDL URL</strong> Address: </label>
					<input type="text" id="url" name="url"/><a><font color="red"> **</font></a>
				</div></br>
				<div>
					<label id ="api_key" for="api_key"><strong>api_key</strong>: </label>
					<input type="text" id="api_key" name="api_key"/><a><font color="red"> **</font></a>
                    <input type="hidden" id="footer" name="footer" value="<?php echo $footer?>"/>
                    <p><label id="dry_run" for="dry_run"><strong> Dry Run: </strong>: </label>
                    	<select name="dry_run">
                    		<option value ="true">true</option>
                    		<option value ="">false</option>
                    	</select>
                   	</p>

				<p id ="url"><strong><font color="red">** Both</font> Parameters<font color="red"> are required!</font></strong></p>
                <p>
                	<label>Select one of the following 3 options: </label>
                    <input type="submit" id= "Foundations Request" name="" value="Foundations Request" formaction="Request_Builder.php" />
                    <input type="submit" id= "RSF K-12 Request" name="submit" value="RSF K-12 Request" formaction="K12_Builder.php" />
                    <input type="submit" id= "LLv3 Request" name="submit" value="LLv3 Request" formaction="LLv3_Builder.php" />
               	</p>
                </div>
        </form>
		</div><!-- #content -->
		<div id="footer">
			<?php
				print ("<p><font color=\"green\">$footer | Created by Hugo Mejia | Property of Rosetta Stone&reg;.</font></p>");
			?>
		</div><!-- #footer -->
	</div><!-- #wrapper -->
    </body>
</html>

