<!DOCTYPE html>
<html>
    <head>
        <title>Create_Learner_Response</title>
        <meta charset="UTF-8">
		<link rel="stylesheet" type="text/css" href="style.css" />
    </head>
    <body>
		<div id="wrapper">
			<pre>
			<?php
			$dry_run ="true";
			$api_key = $user_id = $password = $first_name = $middle_name = $last_name = $groud_id = $response_type = $response_code = $language = $extended_error_text = $wrap_ext_error = $error_type = $footer = $resp_first_name = $resp_middle_name = $resp_last_name = $resp_email = $resp_bday = $resp_gender = $resp_tz = $resp_local = $resp_dname = $resp_country = $resp_lanlevel = $resp_curr = $resp_notes = $resp_dr = Null;
                        if (!empty($_POST)){
				if(!empty($_POST['username']) && !empty($_POST['password']) && !empty($_POST['language']) && !empty($_POST['group_id']) && !empty($_POST['wsdl']) && !empty($_POST['api_key'])){
					//required params for this type of request
					$footer = $_POST['footer'];
					$wsdl = $_POST['wsdl'];
					$api_key = $_POST['api_key'];
					$user_id = $_POST['username'];
					$password = $_POST['password'];
					$group_id = $_POST['group_id'];
					$language = $_POST['language'];
					$dry_run = $_POST['dry_run'];
					//print_r($_POST); //Only used for testing

					//none required params for this type of request	
					if (!empty($_POST['fn'])){
						$first_name = $_POST['fn'];
						$resp_first_name = "<li>\"<font color=\"blue\">$first_name</font>\" as <strong>first_name</strong></li>";
					}else{
						$first_name = Null;
					}
                                        
                    if (!empty($_POST['mn'])){
						$middle_name = $_POST['mn'];
						$resp_middle_name = "<li>\"<font color=\"blue\">$middle_name</font>\" as <strong>middle_name</strong></li>";
					}else{
						$middle_name = Null;
					}
					
					if (!empty($_POST['ln'])){
						$last_name = $_POST['ln'];
						$resp_last_name = "<li>\"<font color=\"blue\">$last_name</font>\" as <strong>last_name</strong></li>";
					}else{
						$last_name = Null;
					}				

					if (!empty($_POST['email'])){
						$email = $_POST['email'];
						$resp_email = "<li>\"<font color=\"blue\">$email</font>\" as <strong>email</strong></li>";
					}else{
						$email = Null;
					}

					if (!empty($_POST['birth_date'])){
						$birth_date = $_POST['birth_date'];
						$resp_bday = "<li>\"<font color=\"blue\">$birth_date</font>\" as <strong>birth_date</strong></li>";
					}else{
						$birth_date = Null;
					}
                                        //print ("<br>birth_date: ".$birth_date);

					if (!empty($_POST['gender'])){
						$gender = $_POST['gender'];
						$resp_gender = "<li>\"<font color=\"blue\">$gender</font>\" as <strong>gender</strong></li>";
					}else{
						$gender = Null;
					}
                                        //print ("<br>gender: ".$gender);

					if (!empty($_POST['time_zone'])){
						$time_zone = $_POST['time_zone'];
						$resp_tz = "<li>\"<font color=\"blue\">$time_zone</font>\" as <strong>time_zone</strong></li>";
					}else{
						$time_zone = Null;
					}
					
					if (!empty($_POST['localization'])){
						$localization = $_POST['localization'];
						$resp_local = "<li>\"<font color=\"blue\">$localization</font>\" as <strong>localization</strong></li>";
					}else{
						$localization = Null;
					}

					if (!empty($_POST['display_name'])){
						$display_name = $_POST['display_name'];
						$resp_dname = "<li>\"<font color=\"blue\">$display_name</font>\" as <strong>display_name</strong></li>";
					}else{
						$display_name = Null;
					}
                                        //print ("<br>display_name: ".$display_name);

					if (!empty($_POST['country'])){
						$country = $_POST['country'];
						$resp_country = "<li>\"<font color=\"blue\">$country</font>\" as <strong>country</strong></li>";
					}else{
						$country = Null;
					}

					if (!empty($_POST['language_level'])){
						$language_level = $_POST['language_level'];
						$resp_lanlevel = "<li>\"<font color=\"blue\">$language_level</font>\" as <strong>language_level</strong></li>";
					}else{
						$language_level = Null;
					}

					if (!empty($_POST['curriculum'])){
						$curriculum = $_POST['curriculum'];
						$resp_curr = "<li>\"<font color=\"blue\">$curriculum</font>\" as <strong>curriculum</strong></li>";
					}else{
						$curriculum = Null;
					}

					if (!empty($_POST['notes'])){
						$notes = $_POST['notes'];
						$resp_notes = "<li>\"<font color=\"blue\">$notes</font>\" as <strong>notes</strong></li>";

					}else{
						$notes = Null;
					}

					if ($dry_run == ""){
						$resp_dr = "<li>And \"<font color=\"blue\">false </font>\" for <strong>dry_run</strong></li>";
					}else{
						$resp_dr = "<li>And \"<font color=\"blue\">true </font>\" for <strong>dry_run</strong></li>";
					}
                    
					$soaperror = False;
					require 'CreateLearner_Class.php';
					try {
						//$client = new SoapClient($wsdl, array('trace' => $trace, 'keep_alive' => false));//Initializing Soap Call
						$client = new SoapClient($wsdl); //Initializing Soap Call
						} catch(SoapFault $exception) {
								$soaperror = True;
								//print_r($exception);
								//$soapmessage = ($exception->getMessage()); would print the raw response
								$soapmessage = wordwrap($exception->getMessage(), 75, "\n", true);
						}
					$create = new CreateLearner($api_key, $user_id, $password, $first_name, $middle_name, $last_name, $email, $birth_date, $gender, $time_zone, $localization, $display_name, $country, $group_id, $language, $language_level, $curriculum, $notes, $dry_run);
					$params = array($create);
                                        if ($soaperror){
                                            echo "<h2><font color=\"blue\">Please Make sure you are typing the correct WSDL URL</font></h2><hr>";
                                            echo "<h3>Soap Error Message:</h3>";
                                            echo "<div id=\"url\"><font color=\"red\">$soapmessage</font></div>";
                                            //echo wordwrap(phpinfo(), 75,true);
                                        }else{
											//print_r ($params);//Only used for testing
                                            $response = $client->__soapCall("CreateLearner", $params);
                                            $response_type = $response->response_type;
                                            $response_code = $response->response_code;
                                            if ($response_type == "success"){
                                                //print_r($response); //just used for testing
                                            	//passing object values to a variables for displaying on page;
                                            }else{
                                                //echo "Error Occurred! ";
                                            }
                                                //The following part of this page is just for display:
                                                echo "<div id=\"header\"><h1>Request Response from: </h1></div><!-- #header -->";
                                                echo "<ul style=\"list-style-type:none\"><li><font color=\"blue\">$wsdl</font></li></ul>";
                                                echo "<h1>Using CreateLeaner with these parameters:</h1><ul style=\"list-style-type:square\"><li>\"<font color=\"blue\">$api_key</font>\" as <strong>api_key</strong></li><li>\"<font color=\"blue\">$user_id</font>\" as <strong>user_id</strong></li><li>\"<font color=\"blue\">$language</font>\" as <strong>language</strong> code</li><li>\"<font color=\"blue\">$group_id</font>\" as <strong>group_id</strong></li><li>\"<font color=\"blue\">$password</font>\" as <strong>password</strong></li>$resp_first_name$resp_middle_name$resp_last_name$resp_email$resp_bday$resp_gender$resp_tz$resp_local$resp_dname$resp_country$resp_lanlevel$resp_curr$resp_notes$resp_dr</ul>";
                                                echo "<hr>";	
                                            if ($response_type == "error"){
                                                    $extended_error_text = $response->error->extended_error_text;
                                                    if ($extended_error_text == null){
                                                        $extended_error_text = $response->error->error_text;
                                                        $error_number = $response->error->error_number;
                                                    }else{
                                                        $error_number = $response->error->error_number;  
                                                    }

                                                    $wrap_ext_error = wordwrap($extended_error_text, 66, "\n", true);
                                                    $error_type = $response->error->error_type;
                                                    echo "<h2>An <font color=\"red\">error</font> has occurred:</h2><div>";
                                                    echo "<ul style=\"list-style-type:square\"><li>Error Description: \"<font color=\"red\">$wrap_ext_error</font>\"</li><li>Error Type = \"<font color=\"red\">$error_type</font>\"</li><li>Error Number = \"<font color=\"red\">$error_number</font>\"</li></ul></div>";
                                                            }
                                            else {
                                                    echo "<h2>The \"response_type\" was: <font color=\"blue\"><strong>$response_type</strong></font></h2><h2>The \"response_code\" was: <font color=\"blue\"><strong>$response_code</strong></font></h2>";
                                                    //echo "<ul style=\"list-style-type:none\"><li><font color=\"blue\"><strong>$response_type</strong></font></li></ul>";
                                                    }
                                        }
				}else{
					 echo "</br></br><h1><font color=\"red\">Missing Parameter values!</font></h1><h3>You must enter all required Parameters to make this type of SOAP request.</h3>";}
			}else{
				echo "</br></br><h1><font color=\"red\">ACCESS DENIED</font></h1><h3>This page is not supposed to be accessed directly</h3>";}
			?>
			</pre>
		<!--[if IE]>
		<div id="IE11footer">			
		<![endif]-->
		<div id="footer">
			<footer>
                         <?php
                            print ("<p><font color=\"green\">$footer | Created by Hugo Mejia | Property of Rosetta Stone&reg;.</font></p>");
			?>
			</footer>
		</div><!-- #footer -->
		</div><!-- #IE11footer-->
		</div><!-- #wrapper -->
	</body>
</html>
